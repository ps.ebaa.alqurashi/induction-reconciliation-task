package com.progressoft.jip8;

import com.progressoft.jip8.exceptions.FileImportException;
import com.progressoft.jip8.exceptions.JSONFileException;
import com.progressoft.jip8.exceptions.TableCreationException;
import com.progressoft.jip8.reader.*;
import com.progressoft.jip8.exceptions.FileNotCorrectException;
import com.progressoft.jip8.validation.InputsValidator;
import com.progressoft.jip8.validation.SourceFile;
import com.progressoft.jip8.validation.TargetFile;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.format.DateTimeParseException;

public class ReconciliationSystemTest {
    private HikariDataSource dataSource = new HikariDataSource();

    @Test
    public void givenNullSourceAndTargetInputs_whenConstructing_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new InputsValidator().validate(null, null));
        Assertions.assertEquals("source and target are null", exception.getMessage());
    }

    @Test
    public void givenNullPathsAndFormats_whenConstructing_thenThrowNullPointerException() {

        SourceFile sourceFile = new SourceFile(null, null);
        TargetFile targetFile = new TargetFile(null, null);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("Null inputs", exception.getMessage());
    }

    @Test
    public void givenInvalidSourcePath_whenConstructing_thenThrowExceptions() {

        Path path = Paths.get(".", "src/test/resources/input-files/bank-transactions.csv");
        Path notExistPath = Paths.get(".", "Abc.txt");
        SourceFile sourceFile = new SourceFile(notExistPath, "csv");
        TargetFile targetFile = new TargetFile(path, "csv");

        FileNotFoundException exception = Assertions.assertThrows(FileNotFoundException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("source path doesn't a file", exception.getMessage());

        Path directory = Paths.get(".");
        SourceFile sourceFile1 = new SourceFile(directory, "csv");
        IllegalArgumentException illegal = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new InputsValidator().validate(sourceFile1, targetFile));
        Assertions.assertEquals("source path is directory", illegal.getMessage());
    }

    @Test
    public void givenDirectorySourcePath_whenConstructing_thenThrowIllegalArgumentException() {
        Path directory = Paths.get(".");
        Path path = Paths.get(".", "src/test/resources/input-files/bank-transactions.csv");
        SourceFile sourceFile = new SourceFile(directory, "csv");
        TargetFile targetFile = new TargetFile(path, "csv");

        IllegalArgumentException illegal = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("source path is directory", illegal.getMessage());
    }

    @Test
    public void givenInvalidTargetPath_whenConstructing_thenThrowExceptions() {
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        Path notExistPath = Paths.get(".", "Abc.txt");
        SourceFile sourceFile = new SourceFile(path, "csv");
        TargetFile targetFile = new TargetFile(notExistPath, "csv");
        FileNotFoundException exception = Assertions.assertThrows(FileNotFoundException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("target path doesn't a file", exception.getMessage());
    }

    @Test
    public void givenTargetDirectory_whenConstructing_thenThrowIllegalArgumentException() {
        Path directory = Paths.get(".");
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        SourceFile sourceFile = new SourceFile(path, "csv");
        TargetFile targetFile = new TargetFile(directory, "csv");

        IllegalArgumentException illegal = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("target path is directory", illegal.getMessage());
    }

    @Test
    public void givenCSVFile_whenRead_theReadTheContentOfFile() throws IOException {
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        CSVReader csvReader = new CSVReader(path);
        String[] records = csvReader.readFromCSVFile();
        for (int index = 0; index < records.length; index++) {
            System.out.print(records[index] + " ");
        }
    }

    @Test
    public void givenCsvFile_whenValidateDateColumnFormat_thenThrowDateTimeParseException() throws FileNotFoundException {
        Path path = Paths.get(getClass().getClassLoader().getResource("testDate.csv").getPath());
        CSVReader csvReader = new CSVReader(path);
        DateTimeParseException dateException = Assertions.assertThrows(DateTimeParseException.class,
                () -> csvReader.readFromCSVFile());
        Assertions.assertEquals("Incorrect date format", dateException.getMessage());
    }

    @Test
    public void givenCsvFile_whenValidateUniqueId_thenThrowFileNotCorrectException() throws FileNotFoundException {
        Path path = Paths.get(getClass().getClassLoader().getResource("idTest.csv").getPath());
        CSVReader csvReader = new CSVReader(path);
        csvReader.readFromCSVFile();
        FileNotCorrectException exception = Assertions.assertThrows(FileNotCorrectException.class,
                csvReader::validateId);
        Assertions.assertEquals("file contains duplicate Id", exception.getMessage());
    }

    @Test
    public void givenCsvFile_whenValidateNullId_thenThrowNullPointerException() throws FileNotFoundException {
        Path path = Paths.get(getClass().getClassLoader().getResource("nullIdTest.csv").getPath());
        CSVReader csvReader = new CSVReader(path);
        csvReader.readFromCSVFile();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                csvReader::validateId);
        Assertions.assertEquals("primary key can't be null", exception.getMessage());
    }

    @Test
    public void givenCsvFile_whenValidateAmountColumnFormat_thenThrowNumberFormatException() throws FileNotFoundException {
        Path path = Paths.get(getClass().getClassLoader().getResource("testAmount.csv").getPath());
        CSVReader csvReader = new CSVReader(path);
        NumberFormatException exception = Assertions.assertThrows(NumberFormatException.class,
                csvReader::readFromCSVFile);
        Assertions.assertEquals("Amount is not a number", exception.getMessage());
    }

    @Test
    public void givenJsonFile_whenValidateUniqueId_thenThrowFileNotCorrectException() {
        Path path = Paths.get(getClass().getClassLoader().getResource("duplicateTransactionsIds.json").getPath());
        JSONReader jsonReader = new JSONReader(path);
        Path path1 = jsonReader.convertToCsv(path.toString(),Paths.get(getClass().getClassLoader().getResource("jsontoCsv.csv").getPath()));
        jsonReader.readConvertedJsonAsCSV(path1);

        FileNotCorrectException exception = Assertions.assertThrows(FileNotCorrectException.class,
                jsonReader::validateId);
        Assertions.assertEquals("file contains duplicate Id", exception.getMessage());
    }

    @Test
    public void givenJsonFile_whenValidateEmptyId_thenThrowNullPointerException() {
        Path path = Paths.get(getClass().getClassLoader().getResource("null_Id.json").getPath());
        Path jsonPath = Paths.get(getClass().getClassLoader().getResource("jsontoCsv.csv").getPath());

        JSONReader jsonReader = new JSONReader(path);
        jsonReader.convertToCsv(path.toString(), jsonPath);
        jsonReader.readConvertedJsonAsCSV(jsonPath);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                jsonReader::validateId);
        Assertions.assertEquals("primary key can't be null", exception.getMessage());
    }

    @Test
    public void givenJsonFile_whenValidateAmount_thenThrowNumberFormatException() throws IOException {
        Path path = Paths.get(getClass().getClassLoader().getResource("amountJson.json").getPath());
        Path jsonPath = Paths.get(getClass().getClassLoader().getResource("jsontoCsv.csv").getPath());

        JSONReader jsonReader = new JSONReader(path);
        jsonReader.convertToCsv(path.toString(), jsonPath);

        NumberFormatException exception = Assertions.assertThrows(NumberFormatException.class,
                () -> jsonReader.readConvertedJsonAsCSV(jsonPath));
        Assertions.assertEquals("Amount is not a number", exception.getMessage());
    }

    @Test
    public void givenJsonFile_whenValidateDate_thenThrowDateTimeParseException() {
        Path path = Paths.get(getClass().getClassLoader().getResource("dateTest.json").getPath());
        Path jsonPath = Paths.get(getClass().getClassLoader().getResource("jsontoCsv.csv").getPath());

        JSONReader jsonReader = new JSONReader(path);
        jsonReader.convertToCsv(path.toString(), jsonPath);

        DateTimeParseException exception = Assertions.assertThrows(DateTimeParseException.class,
                () -> jsonReader.readConvertedJsonAsCSV(jsonPath));
        Assertions.assertEquals("Incorrect date format", exception.getMessage());
    }

    @Test
    public void givenInvalidFormatJson_whenConvertToCSV_thenThrowJsonProcessingExcepion() {
        Path path = Paths.get(getClass().getClassLoader().getResource("wrongFormat.json").getPath());
        Path jsonPath = Paths.get(getClass().getClassLoader().getResource("jsontoCsv.csv").getPath());

        JSONReader jsonReader = new JSONReader(path);

        JSONFileException exception = Assertions.assertThrows(JSONFileException.class,
                () -> jsonReader.convertToCsv(path.toString(), jsonPath)
        );
        Assertions.assertEquals("can't convert json to csv", exception.getMessage());
    }

    @Test
    public void givenNullPath_whenCreateCSVReader_thenThrowNullPointerException() {

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new CSVReader(null));
        Assertions.assertEquals("null path", exception.getMessage());
    }

    @Test
    public void givenInvalidPath_whenCreateCSVReader_thenThrowFileNotFoundException() {

        FileNotFoundException exception = Assertions.assertThrows(FileNotFoundException.class,
                () -> new CSVReader(Paths.get("abc")));
        Assertions.assertEquals("file not found", exception.getMessage());
    }

    @Test
    public void givenDirectory_whenCreateCSVReader_thenThrowIllegalArgumentException() {

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CSVReader(Paths.get(".")));
        Assertions.assertEquals("Not file", exception.getMessage());
    }

    @BeforeEach
    public void initializeDatabase() throws IOException, TableCreationException {
        Path h2 = Files.createTempDirectory("h2");
        dataSource.setJdbcUrl("jdbc:h2:file:" + h2);
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");
    }

    @Test
    public void givenCsvSourceFileWithJsonFormat_whenTakeValueFromUser_thenThrowIllegalArgumentException() {
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());

        SourceFile sourceFile = new SourceFile(path, "json");
        TargetFile targetFile = new TargetFile(path, "csv");

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("source format isn't compatible with source file", exception.getMessage());
    }

    @Test
    public void giveCSVTargetFileWithJsonFormat_whenTakeValueFromUser_thenThrowIllegalArgumentException() {
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());

        SourceFile sourceFile = new SourceFile(path, "csv");
        TargetFile targetFile = new TargetFile(path, "json");

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("target format isn't compatible with target file", exception.getMessage());
    }

    @Test
    public void givenJsonTargetFileWithCSVFormat_whenTakeValueFromUser_thenThrowIllegalArgumentException() {
        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").getPath());

        SourceFile sourceFile = new SourceFile(sourcePath, "csv");
        TargetFile targetFile = new TargetFile(targetPath, "csv");

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("target format isn't compatible with target file", exception.getMessage());
    }

    @Test
    public void giveJsonSourceFileWithCSVFormat_whenTakeValueFromUser_thenThrowIllegalArgumentException() {
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").getPath());

        SourceFile sourceFile = new SourceFile(sourcePath, "csv");
        TargetFile targetFile = new TargetFile(targetPath, "csv");

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new InputsValidator().validate(sourceFile, targetFile));
        Assertions.assertEquals("source format isn't compatible with source file", exception.getMessage());
    }


    @Test
    public void givenCSvSourceFile_thenImportIt() throws IOException {

        insertSourceFileAsCSV();
        try (Connection con = dataSource.getConnection()) {
            try (Statement st = con.createStatement()) {
                ResultSet rs = getSourceResultSet(st);
                rs.next();
                Assertions.assertEquals("TR-47884222201", rs.getString(1));
                Assertions.assertEquals("140.00", rs.getString(2));
                Assertions.assertEquals("USD", rs.getString(3));
                Assertions.assertEquals("2020-01-20", rs.getString(4));
                rs.next();
                Assertions.assertEquals("TR-47884222202", rs.getString(1));
                Assertions.assertEquals("20.000", rs.getString(2));
                Assertions.assertEquals("JOD", rs.getString(3));
                Assertions.assertEquals("2020-01-22", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222203", rs.getString(1));
                Assertions.assertEquals("5000.000", rs.getString(2));
                Assertions.assertEquals("JOD", rs.getString(3));
                Assertions.assertEquals("2020-01-25", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222204", rs.getString(1));
                Assertions.assertEquals("1200.000", rs.getString(2));
                Assertions.assertEquals("JOD", rs.getString(3));
                Assertions.assertEquals("2020-01-31", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222205", rs.getString(1));
                Assertions.assertEquals("60.000", rs.getString(2));
                Assertions.assertEquals("JOD", rs.getString(3));
                Assertions.assertEquals("2020-02-02", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222206", rs.getString(1));
                Assertions.assertEquals("500.00", rs.getString(2));
                Assertions.assertEquals("USD", rs.getString(3));
                Assertions.assertEquals("2020-02-10", rs.getString(4));

            }
        } catch (SQLException e) {
            throw new FileImportException("error in importing", e);
        }

    }

    @Test
    public void givenJsonTargetFile_thenImportIt() throws IOException, TableCreationException {
        insertTargetFileAsJson();
        try (Connection con = dataSource.getConnection()) {
            try (Statement st = con.createStatement()) {
                ResultSet rs = getTargetResultSet(st);
                rs.next();
                Assertions.assertEquals("TR-47884222201", rs.getString(1));
                Assertions.assertEquals("140.00", rs.getString(2));
                Assertions.assertEquals("USD", rs.getString(3));
                Assertions.assertEquals("2020-01-20", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222205", rs.getString(1));
                Assertions.assertEquals("60.000", rs.getString(2));
                Assertions.assertEquals("JOD", rs.getString(3));
                Assertions.assertEquals("2020-02-03", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222206", rs.getString(1));
                Assertions.assertEquals("500.00", rs.getString(2));
                Assertions.assertEquals("USD", rs.getString(3));
                Assertions.assertEquals("2020-02-10", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222202", rs.getString(1));
                Assertions.assertEquals("30.000", rs.getString(2));
                Assertions.assertEquals("JOD", rs.getString(3));
                Assertions.assertEquals("2020-01-22", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222217", rs.getString(1));
                Assertions.assertEquals("12000.000", rs.getString(2));
                Assertions.assertEquals("JOD", rs.getString(3));
                Assertions.assertEquals("2020-02-14", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222203", rs.getString(1));
                Assertions.assertEquals("5000.000", rs.getString(2));
                Assertions.assertEquals("JOD", rs.getString(3));
                Assertions.assertEquals("2020-01-25", rs.getString(4));

                rs.next();
                Assertions.assertEquals("TR-47884222245", rs.getString(1));
                Assertions.assertEquals("420.00", rs.getString(2));
                Assertions.assertEquals("USD", rs.getString(3));
                Assertions.assertEquals("2020-01-12", rs.getString(4));
            }
        } catch (SQLException e) {
            throw new FileImportException("error in importing", e);
        }
    }

    void insertSourceFileAsCSV() throws IOException {
        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        Path file = sourcePath;
        CSVFileImporter importer = new CSVFileImporter(file, dataSource);
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(dataSource);
        databaseInitializer.deleteSourceTable();
        databaseInitializer.createSourceTable();
        importer.importFile("SOURCE", "csv");
    }

    void insertTargetFileAsJson() throws IOException {
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").getPath());
        Path file = targetPath;
        JSONReader jsonReader = new JSONReader(file);
        Path path = jsonReader.convertToCsv(file.toString(), Paths.get(".","target", "jsontoCsv.csv"));
        CSVFileImporter importer = new CSVFileImporter(path, dataSource);
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(dataSource);
        databaseInitializer.deleteTargetTable();
        databaseInitializer.createTargetTable();
        importer.importFile("TARGET", "json");
    }


    private ResultSet getSourceResultSet(Statement st) throws SQLException {
        return st.executeQuery("SELECT * FROM SOURCE");
    }

    private ResultSet getTargetResultSet(Statement st) throws SQLException {
        return st.executeQuery("SELECT * FROM TARGET");
    }

}
