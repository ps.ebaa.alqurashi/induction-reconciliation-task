package com.progressoft.jip8;

import com.progressoft.jip8.exceptions.TableCreationException;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;

import com.progressoft.jip8.reader.DatabaseInitializer;

public class DatabaseIntializerTest {
    // TODO assert the happy scenario by check if tables was created ..done
    HikariDataSource dataSource = new HikariDataSource();

    @BeforeEach
    public void initializeDatabase() throws IOException, TableCreationException {
        Path h2 = Files.createTempDirectory("h2");
        dataSource.setJdbcUrl("jdbc:h2:file:" + h2);
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");
    }

    @Test
    public void givenNullDataSource_whenCreateSourceTable_thenThrowTableCreationException() {
        DatabaseInitializer database = new DatabaseInitializer(null);
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> database.createSourceTable());
        Assertions.assertEquals("Null dataSource, can't create source table", exception.getMessage());

    }

    @Test
    public void givenNullDataSource_whenCreateTargetTable_thenThrowTableCreationException() {
        DatabaseInitializer database = new DatabaseInitializer(null);

        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> database.createTargetTable());
        Assertions.assertEquals("Null dataSource, can't create target table", exception.getMessage());
    }

    @Test
    public void givenDataSource_whenCreateTables_thenCreateIt() {
        DatabaseInitializer database = new DatabaseInitializer(dataSource);
        database.createSourceTable();
        database.createTargetTable();
        ArrayList<String> listOfTable = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData md = connection.getMetaData();
            ResultSet rs = md.getTables(null, null, "%", null);
            while (rs.next()) {
                if (rs.getString(4).equalsIgnoreCase("TABLE")) {
                    listOfTable.add(rs.getString(3));
                }
            }
            Assertions.assertTrue(listOfTable.get(0).equals("SOURCE"));
            Assertions.assertTrue(listOfTable.get(1).equals("TARGET"));
        } catch (SQLException e) {
            throw new IllegalStateException("error in connection, Can't get metadata", e);
        }

    }
    @Test
    public void givenNullDataSource_whenDeleteTargetTable_thenThrowTableCreationException() {
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(null);
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> databaseInitializer.deleteSourceTable());
        Assertions.assertEquals("Null dataSource", exception.getMessage());

    }

    @Test
    public void givenNullDataSource_whenDeleteSourceTable_thenThrowTableCreationException() {
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(null);
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> databaseInitializer.deleteTargetTable());
        Assertions.assertEquals("Null dataSource", exception.getMessage());
    }

}
