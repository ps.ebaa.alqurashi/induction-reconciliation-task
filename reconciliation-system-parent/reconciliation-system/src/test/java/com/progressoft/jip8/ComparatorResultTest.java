package com.progressoft.jip8;

import com.progressoft.jip8.ResultWriter.CSVWriter;
import com.progressoft.jip8.comparator.Comparator;
import com.progressoft.jip8.comparator.ComparatorResult;
import com.progressoft.jip8.exceptions.TableCreationException;
import com.progressoft.jip8.reader.CSVFileImporter;
import com.progressoft.jip8.reader.DatabaseInitializer;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class ComparatorResultTest {
    HikariDataSource dataSource = new HikariDataSource();
    DatabaseInitializer database = new DatabaseInitializer(dataSource);

    @BeforeEach
    public void initializeDatabase() throws IOException, TableCreationException {
        Path h2 = Files.createTempDirectory("h2");
        dataSource.setJdbcUrl("jdbc:h2:file:" + h2);
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");

        database.deleteSourceTable();
        database.deleteTargetTable();
        database.createSourceTable();
        database.createTargetTable();
    }

    @Test
    public void givenTwoCsvFile_whenCompare_thenReturnComparatorResult() throws IOException {
        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());

        CSVFileImporter importerSource = new CSVFileImporter(sourcePath, dataSource);
        importerSource.importFile("SOURCE", "csv");

        CSVFileImporter importerTarget = new CSVFileImporter(targetPath, dataSource);
        importerTarget.importFile("TARGET", "csv");

        Comparator comparator = new Comparator(dataSource);
        ComparatorResult comparatorResult = comparator.compareTransRecords();

        List<String> matching = comparatorResult.getMatchingList();
        List<String> misMatching = comparatorResult.getMisMatchingList();
        List<String> missing = comparatorResult.getMissingList();

        Assertions.assertEquals(0,misMatching.size());
        Assertions.assertEquals(0,missing.size());

        matching.removeAll(Collections.singleton(","));

        List<String> record1 = matching.subList(0, 4);
        Assertions.assertEquals("TR-47884222201", record1.get(0));
        Assertions.assertEquals("140.00", record1.get(1));
        Assertions.assertEquals("USD", record1.get(2));
        Assertions.assertEquals("2020-01-20", record1.get(3));

        List<String> record2 = matching.subList(5, 9);
        Assertions.assertEquals("TR-47884222202", record2.get(0));
        Assertions.assertEquals("20.000", record2.get(1));
        Assertions.assertEquals("JOD", record2.get(2));
        Assertions.assertEquals("2020-01-22", record2.get(3));


        List<String> record3 = matching.subList(10, 14);
        Assertions.assertEquals("TR-47884222203", record3.get(0));
        Assertions.assertEquals("5000.000", record3.get(1));
        Assertions.assertEquals("JOD", record3.get(2));
        Assertions.assertEquals("2020-01-25", record3.get(3));

        List<String> record4 = matching.subList(15, 19);
        Assertions.assertEquals("TR-47884222204", record4.get(0));
        Assertions.assertEquals("1200.000", record4.get(1));
        Assertions.assertEquals("JOD", record4.get(2));
        Assertions.assertEquals("2020-01-31", record4.get(3));

        List<String> record5 = matching.subList(20, 24);
        Assertions.assertEquals("TR-47884222205", record5.get(0));
        Assertions.assertEquals("60.000", record5.get(1));
        Assertions.assertEquals("JOD", record5.get(2));
        Assertions.assertEquals("2020-02-02", record5.get(3));

        List<String> record6 = matching.subList(25, 30);
        Assertions.assertEquals("TR-47884222206", record6.get(0));
        Assertions.assertEquals("500.00", record6.get(1));
        Assertions.assertEquals("USD", record6.get(2));
        Assertions.assertEquals("2020-02-10", record6.get(3));


    }
}
