package com.progressoft.jip8;

import com.progressoft.jip8.ResultWriter.CSVWriter;
import com.progressoft.jip8.ResultWriter.FileWriter;
import com.progressoft.jip8.comparator.Comparator;
import com.progressoft.jip8.comparator.ComparatorResult;
import com.progressoft.jip8.exceptions.TableCreationException;
import com.progressoft.jip8.reader.CSVFileImporter;
import com.progressoft.jip8.reader.CSVReader;
import com.progressoft.jip8.reader.DatabaseInitializer;
import com.progressoft.jip8.reader.JSONReader;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CSVWriterTest {
    HikariDataSource dataSource = new HikariDataSource();
    DatabaseInitializer database = new DatabaseInitializer(dataSource);

    @BeforeEach
    public void initializeDatabase() throws IOException, TableCreationException {
        Path h2 = Files.createTempDirectory("h2");
        dataSource.setJdbcUrl("jdbc:h2:file:" + h2);
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");
        database.deleteSourceTable();
        database.deleteTargetTable();
        database.createSourceTable();
        database.createTargetTable();
    }

    @Test
    public void givenCSVAndJsonFiles_whenWrite_thenReadTheContent() throws IOException {
        File matchFile;
        File missFile;
        File misMatchFile;

        try {
            matchFile = File.createTempFile("match", null);
            missFile=File.createTempFile("miss",null);
            misMatchFile=File.createTempFile("misMatch",null);
        } catch (IOException e) {
            throw new IllegalStateException("can't create the matchFile ", e);
        }

        Path matchPath = matchFile.toPath();
        Path misMatchPath = misMatchFile.toPath();
        Path missPath = missFile.toPath();

        Path sourcePath = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        Path targetPath = Paths.get(getClass().getClassLoader().getResource("online-banking-transactions.json").getPath());

        CSVFileImporter importerSource = new CSVFileImporter(sourcePath, dataSource);
        importerSource.importFile("SOURCE", "csv");

        JSONReader reader = new JSONReader(targetPath);
        Path convertedFilePath = Paths.get(getClass().getClassLoader().getResource("jsontoCsv.csv").getPath());
        targetPath = reader.convertToCsv(targetPath.toString(), convertedFilePath);

        CSVFileImporter importerTarget = new CSVFileImporter(targetPath, dataSource);
        importerTarget.importFile("TARGET", "json");

        Comparator comparator = new Comparator(dataSource);
        ComparatorResult comparatorResult = comparator.compareTransRecords();

        List<String> matching = comparatorResult.getMatchingList();
        List<String> misMatching = comparatorResult.getMisMatchingList();
        List<String> missing = comparatorResult.getMissingList();

        CSVWriter writer = new CSVWriter();
        writer.writeMatchingRecords(matching, matchPath);
        writer.writeMissMatchingAndMissingRecords(misMatching,misMatchPath);
        writer.writeMissMatchingAndMissingRecords(missing,missPath);

        CSVReader matchRowsReader = new CSVReader(matchPath);
        List<String[]> matchRows = matchRowsReader.readRecords();

        CSVReader misMatchReader=new CSVReader(misMatchPath);
        List<String[]> misMatchRows = misMatchReader.readRecords();

        CSVReader missReader =new CSVReader(missPath);
        List<String[]> missRows = missReader.readRecords();


        String[] value;
        int length = matchRows.get(0).length;
        Assertions.assertEquals(4, length);

        value = matchRows.get(0);
        Assertions.assertEquals("TR-47884222201", value[0]);
        Assertions.assertEquals("140.00", value[1]);
        Assertions.assertEquals("USD", value[2]);
        Assertions.assertEquals("2020-01-20", value[3]);

        value = matchRows.get(1);
        Assertions.assertEquals("TR-47884222203", value[0]);
        Assertions.assertEquals("5000.000", value[1]);
        Assertions.assertEquals("JOD", value[2]);
        Assertions.assertEquals("2020-01-25", value[3]);

        value = matchRows.get(2);
        Assertions.assertEquals("TR-47884222206", value[0]);
        Assertions.assertEquals("500.00", value[1]);
        Assertions.assertEquals("USD", value[2]);
        Assertions.assertEquals("2020-02-10", value[3]);

        //missmatch
        value = misMatchRows.get(0);
        Assertions.assertEquals("SOURCE",value[0]);
        Assertions.assertEquals("TR-47884222202", value[1]);
        Assertions.assertEquals("20.000", value[2]);
        Assertions.assertEquals("JOD", value[3]);
        Assertions.assertEquals("2020-01-22", value[4]);

        value = misMatchRows.get(1);
        Assertions.assertEquals("TARGET",value[0]);
        Assertions.assertEquals("TR-47884222202", value[1]);
        Assertions.assertEquals("30.000", value[2]);
        Assertions.assertEquals("JOD", value[3]);
        Assertions.assertEquals("2020-01-22", value[4]);

        value = misMatchRows.get(2);
        Assertions.assertEquals("SOURCE",value[0]);
        Assertions.assertEquals("TR-47884222205", value[1]);
        Assertions.assertEquals("60.000", value[2]);
        Assertions.assertEquals("JOD", value[3]);
        Assertions.assertEquals("2020-02-02", value[4]);

        value = misMatchRows.get(3);
        Assertions.assertEquals("TARGET",value[0]);
        Assertions.assertEquals("TR-47884222205", value[1]);
        Assertions.assertEquals("60.000", value[2]);
        Assertions.assertEquals("JOD", value[3]);
        Assertions.assertEquals("2020-02-03", value[4]);

        value = missRows.get(0);
        Assertions.assertEquals("SOURCE",value[0]);
        Assertions.assertEquals("TR-47884222204", value[1]);
        Assertions.assertEquals("1200.000", value[2]);
        Assertions.assertEquals("JOD", value[3]);
        Assertions.assertEquals("2020-01-31", value[4]);

        value=missRows.get(1);
        Assertions.assertEquals("TARGET",value[0]);
        Assertions.assertEquals("TR-47884222217", value[1]);
        Assertions.assertEquals("12000.000", value[2]);
        Assertions.assertEquals("JOD", value[3]);
        Assertions.assertEquals("2020-02-14", value[4]);

        value=missRows.get(2);
        Assertions.assertEquals("TARGET", value[0]);
        Assertions.assertEquals("TR-47884222245", value[1]);
        Assertions.assertEquals("420.00", value[2]);
        Assertions.assertEquals("USD", value[3]);
        Assertions.assertEquals("2020-01-12", value[4]);
    }
}
