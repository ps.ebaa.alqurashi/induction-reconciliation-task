package com.progressoft.jip8;

import com.progressoft.jip8.exceptions.ExcuteSQLQueriesException;
import com.progressoft.jip8.exceptions.FileImportException;
import com.progressoft.jip8.exceptions.TableCreationException;
import com.progressoft.jip8.reader.CSVFileImporter;
import com.progressoft.jip8.exceptions.FileNotCorrectException;
import com.progressoft.jip8.reader.DatabaseInitializer;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

public class CSVFileImporterTest {
    // TODO where is the happy scenario check...Done
    HikariDataSource dataSource = new HikariDataSource();

    @BeforeEach
    public void initializeDatabase() throws IOException, TableCreationException {
        Path h2 = Files.createTempDirectory("h2");
        dataSource.setJdbcUrl("jdbc:h2:file:" + h2);
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");
    }

    @Test
    public void givenNullDataSource_whenImportCSVfile_thenThrowNullPointerException() {
        Path path = Paths.get(getClass().getClassLoader().getResource("idTest.csv").getPath());
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new CSVFileImporter(path, null));
        Assertions.assertEquals("null datasource", exception.getMessage());
    }

    @Test
    public void givenDirectoryPath_whenImportCSVFile_thenThrowIllegalArgumentException() {
        Path dir = Paths.get(".");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CSVFileImporter(dir, dataSource));
        Assertions.assertEquals("not a file", exception.getMessage());
    }

    @Test
    public void givenInvalidPath_whenImportCSVFile_thenThrowFileNotFoundException() {
        Path dir = Paths.get(".", "abc");
        FileNotFoundException exception = Assertions.assertThrows(FileNotFoundException.class, () -> new CSVFileImporter(dir, dataSource));
        Assertions.assertEquals("file not exist", exception.getMessage());
    }

    @Test
    public void givenNullPath_whenImportCSVFile_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new CSVFileImporter(null, null));
        Assertions.assertEquals("null datasource", exception.getMessage());
    }

    @Test
    public void givenEmptyFile_whenReadFileInImportingFile_thenThrowFileImportingFile() {
        Path path = Paths.get(getClass().getClassLoader().getResource("EmptyFile.csv").getPath());
        FileNotCorrectException exception = Assertions.assertThrows(FileNotCorrectException.class,
                () -> new CSVFileImporter(path, dataSource));
        Assertions.assertEquals("Empty file", exception.getMessage());
    }

    @Test
    public void givenValidFileAndDataSource_whenImportFile_thenImportItAndAddToDatabaseTable() {
        Path path = Paths.get(getClass().getClassLoader().getResource("bank-transactions.csv").getPath());
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(dataSource);
        databaseInitializer.createSourceTable();
        try (Connection connection = dataSource.getConnection()) {
            CSVFileImporter importer = new CSVFileImporter(path, dataSource);
            importer.importFile("SOURCE", "csv");
            try (Statement statement = connection.createStatement()) {
                ResultSet rs = statement.executeQuery("SELECT * FROM SOURCE");
              rs.next();
                    //TR-47884222201,140.00,USD,2020-01-20
                    Assertions.assertEquals("TR-47884222201", rs.getString("ID"));
                    Assertions.assertEquals("140.00", rs.getBigDecimal("AMOUNT").toString());
                    Assertions.assertEquals("USD", rs.getString("CURRENCY"));
                    Assertions.assertEquals("2020-01-20", rs.getDate("DATE").toString());

            }
        } catch (IOException e) {
            throw new FileImportException("Error in importing the file", e);
        } catch (SQLException e) {
            throw new ExcuteSQLQueriesException("SOURCE Table isn't exists", e);
        }
    }
}