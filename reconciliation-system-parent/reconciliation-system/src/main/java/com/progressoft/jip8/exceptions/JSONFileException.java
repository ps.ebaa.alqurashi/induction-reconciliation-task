package com.progressoft.jip8.exceptions;

import com.fasterxml.jackson.core.JsonProcessingException;

public class JSONFileException extends RuntimeException {
    public JSONFileException(String message, Exception e) {

        super(message,e);
    }
}
