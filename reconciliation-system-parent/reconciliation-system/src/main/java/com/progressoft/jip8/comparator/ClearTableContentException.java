package com.progressoft.jip8.comparator;

public class ClearTableContentException extends RuntimeException {
    public ClearTableContentException( Exception cause) {
        super( cause);
    }
}
