package com.progressoft.jip8.ResultWriter;

import com.progressoft.jip8.comparator.ComparatorResult;

public abstract class FileWriter {

    public final String generateFiles(ComparatorResult result) {
        writeResultToFiles(result);
        return getResultDirectory();
    }

    abstract void writeResultToFiles(ComparatorResult result);

    abstract String getResultDirectory();


}
