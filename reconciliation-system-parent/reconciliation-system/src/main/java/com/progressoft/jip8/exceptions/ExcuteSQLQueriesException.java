package com.progressoft.jip8.exceptions;


public class ExcuteSQLQueriesException extends RuntimeException {
    public ExcuteSQLQueriesException(String message, Exception e) {
        super(message, e);
    }
}
