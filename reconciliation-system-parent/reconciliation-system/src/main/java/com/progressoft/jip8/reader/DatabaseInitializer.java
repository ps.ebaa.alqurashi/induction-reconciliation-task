package com.progressoft.jip8.reader;

import com.progressoft.jip8.exceptions.TableCreationException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

public class DatabaseInitializer {

    private DataSource dataSource;

    public DatabaseInitializer(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public  void createSourceTable() {
        if (Objects.isNull(dataSource)) {
            throw new NullPointerException("Null dataSource, can't create source table");
        }
        String SQL_CREATE = "CREATE TABLE SOURCE "
                + "( ID VARCHAR(30) NOT NULL PRIMARY KEY," +
                " AMOUNT VARCHAR(30) NOT NULL , CURRENCY VARCHAR(30) NOT NULL , DATE DATE(30) NOT NULL," +
                "FOUND VARCHAR(30) NOT NULL )";

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement =
                         connection.prepareStatement(SQL_CREATE)) {
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new TableCreationException("failed to create source table", e);
        }
    }

    public  void createTargetTable() {
        if (Objects.isNull(dataSource)) {
            throw new NullPointerException("Null dataSource, can't create target table");
        }
        String SQL_CREATE = "CREATE TABLE TARGET ( ID VARCHAR(30) NOT NULL PRIMARY KEY," +
                " AMOUNT VARCHAR(30) NOT NULL , CURRENCY VARCHAR(30) NOT NULL , DATE DATE(30) NOT NULL," +
                "FOUND VARCHAR(30) NOT NULL )";
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(SQL_CREATE)) {
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new TableCreationException("failed in create target table", e);
        }
    }

    public  void deleteSourceTable() {
        if (Objects.isNull(dataSource)) {
            throw new NullPointerException("Null dataSource");
        }
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("DROP TABLE IF EXISTS SOURCE ")) {
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new TableCreationException("failed in delete source table", e);
        }
    }

    public  void deleteTargetTable() {
        if (Objects.isNull(dataSource)) {
            throw new NullPointerException("Null dataSource");
        }
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("DROP TABLE IF EXISTS TARGET ")) {
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new TableCreationException("failed in delete target table", e);
        }
    }
}