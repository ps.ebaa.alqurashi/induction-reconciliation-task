package com.progressoft.jip8.validation;

import java.nio.file.Path;

public class SourceFile {
    private Path sourcePath;
    private String sourceFormat;

    public SourceFile(Path sourcePath, String  sourceFormat) {
        this.sourcePath = sourcePath;
        this.sourceFormat = sourceFormat;
    }

    public Path getSourcePath() {
        return sourcePath;
    }

    public String getSourceFormat() {
        return sourceFormat;
    }
}
