package com.progressoft.jip8.reader;

import com.progressoft.jip8.exceptions.FileImportException;
import com.progressoft.jip8.exceptions.FileNotCorrectException;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Objects;

public class CSVFileImporter {
    private final Path path;
    private final DataSource dataSource;

    public CSVFileImporter(Path path, DataSource dataSource) throws IOException {
        if (Objects.isNull(dataSource))
            throw new NullPointerException("null datasource");
        if (Files.isDirectory(path))
            throw new IllegalArgumentException("not a file");
        if (Files.notExists(path))
            throw new FileNotFoundException("file not exist");
        if (Files.size(path) == 0)
            throw new FileNotCorrectException("Empty file");
        this.path = path;
        this.dataSource = dataSource;
    }

    public void importFile(String tableName, String format) {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            CSVReader csvReader = new CSVReader(path);
            csvReader.readFile(new Importer(connection), tableName,format);
            connection.commit();
        } catch (SQLException | IOException e) {
            throw new FileImportException(" failed in importing file", e);
        }
    }

    private class Importer implements CSVReader.CSVConsumer {
        private Connection connection;

        private Importer(Connection connection) {
            this.connection = connection;
        }

        @Override
        public void accept(String tableName, String[] record) {
            // TODO you should modify the json data to look exactly as the csv record ...Done
            addToTable(tableName, record[0], record[2], record[3], record[5]);
        }

        private void addToTable(String tableName, String id, String amount, String currency, String date) {
            try (PreparedStatement statement =
                         connection.prepareStatement("insert into " + tableName + " values (?,?,?,?,?)")) {
                Currency currencyInstance = Currency.getInstance(currency);
                int digits = currencyInstance.getDefaultFractionDigits();
                BigDecimal amountValue = new BigDecimal(amount).setScale(digits);
                statement.setString(1, id);
                statement.setBigDecimal(2, amountValue);
                statement.setString(3, currency);
                storeDateToDatabase(date, statement);
                statement.setString(5, tableName);
                statement.executeUpdate();
            } catch (SQLIntegrityConstraintViolationException e) {
                System.out.println("transaction id : " + id + " already exists");
            } catch (SQLException e) {
                throw new IllegalStateException("can't insert to " + tableName + " table", e);
            }
        }

        private void storeDateToDatabase(String date, PreparedStatement statement) {
            try {
                if (DateParser.isFormatCorrect("yyyy-MM-dd", date)) {
                    // TODO setDate ..done
                    statement.setDate(4, java.sql.Date.valueOf(date));
                } else {
                    Date parsedDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);
                    Format formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String s = formatter.format(parsedDate);
                    statement.setDate(4, java.sql.Date.valueOf(s));
                }
            } catch (SQLException e) {
                throw new RuntimeException("date doesn't insert in table, problem in parsing", e);
            } catch (ParseException e) {
                throw new IllegalStateException("can't parsing date value", e);
            }
        }
    }
}