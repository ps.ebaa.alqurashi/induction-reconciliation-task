package com.progressoft.jip8.exceptions;


import java.io.IOException;

public class FileException extends RuntimeException {
    public FileException(String message, IOException e) {
        super(message,e);
    }
}
