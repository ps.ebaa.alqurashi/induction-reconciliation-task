package com.progressoft.jip8.exceptions;

public class FileNotCorrectException extends RuntimeException {

    public FileNotCorrectException(String message){
        super(message);
    }
    public FileNotCorrectException(String message,Exception e){
        super(message,e);
    }
}
