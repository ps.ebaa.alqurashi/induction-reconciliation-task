package com.progressoft.jip8.validation;

import java.nio.file.Path;

public class TargetFile {

    private Path targetPath;
    private String  targetFormat;

    public TargetFile(Path targetPath, String targetEx) {
        this.targetPath = targetPath;
        this.targetFormat = targetEx;
    }

    public Path getTargetPath() {
        return targetPath;
    }

    public String getTargetFormat() {
        return targetFormat;
    }
}
