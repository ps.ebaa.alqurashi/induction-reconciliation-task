package com.progressoft.jip8.ResultWriter;

import java.io.IOException;

public class FileWriterException extends RuntimeException {
    public FileWriterException(String message, IOException e) {
        super(message,e);
    }
}
