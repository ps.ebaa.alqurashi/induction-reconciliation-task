package com.progressoft.jip8.reader;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({
        "reference", "description", "amount", "currencyCode", "purpose", "date", "found"
})
public abstract class JsonReorder {

    @JsonProperty("reference")
    private String reference;

    @JsonProperty("description")
    private String description;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("currencyCode")
    private String currencyCode;

    @JsonProperty("purpose")
    private String purpose;

    @JsonProperty("found")
    private String found;

    @JsonProperty("date")
    private String Date;
}

