package com.progressoft.jip8.comparator;

import java.util.List;

public class ComparatorResult {
    List<String> missingList;
    List<String> matchingList;
    List<String> misMatchingList;

    public ComparatorResult(List<String> matchingList, List<String> misMatchingList, List<String> missingList) {
        this.missingList = missingList;
        this.matchingList = matchingList;
        this.misMatchingList = misMatchingList;
    }

    public List<String> getMissingList() {
        return missingList;
    }

    public List<String> getMatchingList() {
        return matchingList;
    }

    public List<String> getMisMatchingList() {
        return misMatchingList;
    }
}