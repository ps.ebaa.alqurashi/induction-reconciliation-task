package com.progressoft.jip8.reader;

import java.io.IOException;
import java.nio.file.Path;

public abstract class FileConverterAdapter {

   abstract Path convertToCsv(String path,Path convertedFilePath) throws IOException;

}
