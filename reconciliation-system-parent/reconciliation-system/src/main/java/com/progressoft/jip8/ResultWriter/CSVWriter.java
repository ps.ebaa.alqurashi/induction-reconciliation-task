package com.progressoft.jip8.ResultWriter;

import com.progressoft.jip8.comparator.ComparatorResult;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CSVWriter extends FileWriter {
    // TODO this shouldn't be a utility static methods, have them instance methods, then inject this writher to comparator ..Done
    private ComparatorResult ComparisonResult;
    private Path resultDirectory;
    private Path matchingPath;
    private Path missMatchingPath;
    private Path missingPath;

    @Override
    public void writeResultToFiles(ComparatorResult result) {
        this.ComparisonResult = result;
        createResultDirectoryAndFiles();
        writeMatchingRecords(this.ComparisonResult.getMatchingList(), matchingPath);
        writeMissMatchingAndMissingRecords(this.ComparisonResult.getMissingList(), missingPath);
        writeMissMatchingAndMissingRecords(this.ComparisonResult.getMisMatchingList(), missMatchingPath);
    }

    @Override
    public String getResultDirectory() {
        return resultDirectory.toString();
    }

    private void createResultDirectoryAndFiles() {
        try {
            resultDirectory = Paths.get(System.getProperty("user.home") + "/result");
            Files.createDirectory(resultDirectory);
            matchingPath = Files.createFile(Paths.get(resultDirectory.toString(), "matching-transactions.csv"));
            missMatchingPath = Files.createFile(Paths.get(resultDirectory.toString(), "misMatched-transaction.csv"));
            missingPath = Files.createFile(Paths.get(resultDirectory.toString(), "missing-transaction.csv"));

        } catch (FileAlreadyExistsException e) {
            throw new RuntimeException("There is already a file named result in " + System.getProperty("user.home"));
        } catch (IOException e) {
            throw new IllegalStateException("Error while generating result files!", e);
        }
    }

    // TODO enhance those methods by accepting ResultSet instead of passing a connection and a statement...Done
    public void writeMissMatchingAndMissingRecords(List<String> list, Path path) {
        try (BufferedWriter bw = Files.newBufferedWriter(path)) {
            PrintWriter printWriter = new PrintWriter(bw);
            printWriter.println("found,transaction id,amount,currency code,value date");
            for (int index = 0; index < list.size(); index++) {
                printWriter.print(list.get(index));
                printWriter.flush();
            }
        } catch (IOException e) {
           throw new FileWriterException("Error n writing records",e);
        }
    }

    public void writeMatchingRecords(List<String> matchingList, Path matchingPath) {
        try (BufferedWriter bw = Files.newBufferedWriter(matchingPath)) {
            PrintWriter printWriter = new PrintWriter(bw);
            printWriter.println("transaction id,amount,currency code,value date");
            for (int index = 0; index < matchingList.size(); index++) {
                printWriter.print(matchingList.get(index));
                printWriter.flush();
            }
        } catch (IOException e) {
           throw new FileWriterException("Error n writing records",e);
        }
    }
}