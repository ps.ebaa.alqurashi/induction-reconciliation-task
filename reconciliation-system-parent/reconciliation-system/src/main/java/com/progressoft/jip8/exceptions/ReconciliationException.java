package com.progressoft.jip8.exceptions;

import java.io.IOException;

public class ReconciliationException extends RuntimeException {
    public ReconciliationException(String message, IOException e) {
    super(message,e);
    }
}
