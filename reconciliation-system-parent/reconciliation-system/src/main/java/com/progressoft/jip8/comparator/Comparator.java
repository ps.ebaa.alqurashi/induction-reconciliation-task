package com.progressoft.jip8.comparator;


import com.progressoft.jip8.exceptions.ExcuteSQLQueriesException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Comparator {
    private DataSource dataSource;

    private List<String> missingList = new ArrayList<>();
    private List<String> matchingList = new ArrayList<>();
    private List<String> misMatchingList = new ArrayList<>();

    public Comparator(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ComparatorResult compareTransRecords() {
        String missingQuery = "SELECT * FROM SOURCE WHERE NOT EXISTS ( SELECT * FROM TARGET WHERE TARGET.ID = SOURCE.ID AND TARGET.CURRENCY = SOURCE.CURRENCY )  UNION SELECT * FROM TARGET WHERE NOT EXISTS ( SELECT * FROM SOURCE WHERE TARGET.ID = SOURCE.ID AND TARGET.CURRENCY = SOURCE.CURRENCY )";

        String missMatchQuery = "SELECT  * FROM TARGET T WHERE EXISTS ( SELECT 1 FROM SOURCE S WHERE ( S.ID = T.ID AND ( S.CURRENCY != T.CURRENCY OR S.AMOUNT != T.AMOUNT OR S.DATE != T.DATE ) ))  UNION  SELECT  * FROM SOURCE S WHERE EXISTS ( SELECT 1 FROM TARGET T WHERE ( S.ID = T.ID AND ( S.CURRENCY != T.CURRENCY OR S.AMOUNT != T.AMOUNT OR S.DATE != T.DATE ) ))";

        String matchingQuery = "SELECT  * FROM SOURCE S WHERE EXISTS ( SELECT 1 FROM TARGET T WHERE" +
                " ( S.ID = T.ID AND S.CURRENCY = T.CURRENCY AND S.AMOUNT = T.AMOUNT AND S.DATE= T.DATE ))";

        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);

            try (Statement st = connection.createStatement()) {
                ResultSet missingRecords = st.executeQuery(missingQuery);
                while (missingRecords.next()) {
                    missingList.add(missingRecords.getString("FOUND"));
                    missingList.add(",");
                    missingList.add(missingRecords.getString("ID"));
                    missingList.add(",");
                    missingList.add(missingRecords.getString("AMOUNT"));
                    missingList.add(",");
                    missingList.add(missingRecords.getString("CURRENCY"));
                    missingList.add(",");
                    Date date = missingRecords.getDate("DATE");
                    missingList.add(date.toString());
                    missingList.add("\n");
                }
                ResultSet missMatchRecords = st.executeQuery(missMatchQuery);
                while (missMatchRecords.next()) {
                    misMatchingList.add(missMatchRecords.getString("FOUND"));
                    misMatchingList.add(",");
                    misMatchingList.add(missMatchRecords.getString("ID"));
                    misMatchingList.add(",");
                    misMatchingList.add(missMatchRecords.getString("AMOUNT"));
                    misMatchingList.add(",");
                    misMatchingList.add(missMatchRecords.getString("CURRENCY"));
                    misMatchingList.add(",");
                    Date date = missMatchRecords.getDate("DATE");
                    misMatchingList.add(date.toString());
                    misMatchingList.add("\n");
                }
                ResultSet matchingRecords = st.executeQuery(matchingQuery);
                while (matchingRecords.next()) {
                    matchingList.add(matchingRecords.getString("ID"));
                    matchingList.add(",");
                    matchingList.add(matchingRecords.getString("AMOUNT"));
                    matchingList.add(",");
                    matchingList.add(matchingRecords.getString("CURRENCY"));
                    matchingList.add(",");
                    Date date = matchingRecords.getDate("DATE");
                    matchingList.add(date.toString());
                    matchingList.add("\n");
                }
                clearTables(connection, "SOURCE");
                clearTables(connection, "TARGET");

                return new ComparatorResult(matchingList, misMatchingList, missingList);

            } catch (SQLSyntaxErrorException e) {
                // TODO don't throw runtimeexception...Done
                throw new ExcuteSQLQueriesException("query syntax isn't correct", e);
            }
        } catch (SQLException e) {
            throw new ExcuteSQLQueriesException("can't get rows from source and target tables", e);
        }
    }

    private void clearTables(Connection connection, String name) {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM " + name)) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new ClearTableContentException(e);
        }
    }
}