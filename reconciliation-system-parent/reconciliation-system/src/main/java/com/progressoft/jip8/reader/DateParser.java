package com.progressoft.jip8.reader;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


public class DateParser {


    public static boolean isFormatCorrect(String format, String value) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        try {
            LocalDate ld = LocalDate.parse(value, formatter);
            String result = ld.format(formatter);
            return result.equals(value);
        } catch (DateTimeParseException exp) {
            return false;
        }
    }

    public static boolean isValidFormat(String format, String value) throws DateTimeParseException {
        LocalDate ld;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        String result;
        try {
            ld = LocalDate.parse(value, formatter);
            result = ld.format(formatter);
            return result.equals(value);
        } catch (DateTimeParseException e) {
            throw new DateTimeParseException("Incorrect date format", value, 4);
        }
    }
}