package com.progressoft.jip8.reader;

import com.progressoft.jip8.exceptions.ExcuteSQLQueriesException;
import com.progressoft.jip8.exceptions.FileImportException;
import com.progressoft.jip8.exceptions.FileNotCorrectException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CSVReader {
    protected Path path;
    static final String JSON = "json";
    private List<String> idList = new ArrayList<>();
    private List<BigDecimal> amountList = new ArrayList<>();

    public CSVReader(Path path) throws FileNotFoundException {
        isValidPath(path);
        this.path = path;
    }

    private void skipHeader(BufferedReader bf) throws IOException {
        bf.readLine();
    }

    private String[] commaSplit(String l) {
        return l.split(",");
    }

    public void readFile(CSVConsumer consumer, String tableName, String format){
        try (BufferedReader bf = Files.newBufferedReader(path)) {
            skipHeader(bf);
            String line;
            while ((line = bf.readLine()) != null) {
                String[] record = commaSplit(line);
                addToList(idList, record[0]);
                addToList(amountList, String.valueOf(new BigDecimal(record[2])));
                if (format.equals(JSON)) {
                    DateParser.isValidFormat("dd/MM/yyyy", record[5]);
                } else {
                    DateParser.isValidFormat("yyyy-MM-dd", record[5]);
                }
                consumer.accept(tableName, record);
            }
        } catch (DateTimeParseException e) {
            throw new DateTimeParseException("Incorrect date format", "column 5 ", 5);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Amount is not a number");
        } catch (IOException e) {
            throw new IllegalArgumentException("can't read csv file", e);
        } catch (SQLException e) {
            throw new ExcuteSQLQueriesException("can't read file correctly",e);
        }
    }


    public String[] readFromCSVFile() {
        Path file = path;
        try (BufferedReader br = Files.newBufferedReader(file)) {
            String line;
            String[] record = null;
            skipHeader(br);
            while ((line = br.readLine()) != null) {
                record = line.split(",");
                addToList(idList, record[0]);
                addToList(amountList, String.valueOf(new BigDecimal(record[2])));
                DateParser.isValidFormat("yyyy-MM-dd", record[5]);
            }
            return record;
        }
        catch (DateTimeParseException e) {
            throw new DateTimeParseException("Incorrect date format", "column 5 ", 5);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Amount is not a number");
        } catch (IOException e) {
            throw new IllegalArgumentException("can't read csv file", e);
        }
    }

    void addToList(List list, String record) {
        list.add(record);
    }

    public void validateId() {
        int listSize = idList.size();
        for (int i = 0; i < listSize; i++) {
            for (int j = i + 1; j < listSize; j++) {
                if (idList.get(i).equals(""))
                    throw new NullPointerException("primary key can't be null");
                if (idList.get(i).equals(idList.get(j)))
                    throw new FileNotCorrectException("file contains duplicate Id");
            }
        }
    }

    public List<String[]> readRecords() {
        List<String[]> content = new ArrayList<>();
        try (BufferedReader bf = Files.newBufferedReader(path)) {
            skipHeader(bf);
            String line;
            String[] record;
            while ((line = bf.readLine()) != null) {
                record = line.split(",");
                content.add(record);
            }
            return content;
        } catch (IOException e) {
            throw new FileImportException("can't read from file", e);
        }

    }

    public interface CSVConsumer {
        void accept(String name, String[] record) throws SQLException;
    }

    private void isValidPath(Path path) throws FileNotFoundException {
        Objects.requireNonNull(path, "null path");
        if (Files.notExists(path))
            throw new FileNotFoundException("file not found");
        if (Files.isDirectory(path))
            throw new IllegalArgumentException("Not file");
    }
}