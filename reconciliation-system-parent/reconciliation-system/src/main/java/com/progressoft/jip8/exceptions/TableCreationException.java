package com.progressoft.jip8.exceptions;

public class TableCreationException extends RuntimeException {
    public TableCreationException(String message, Exception e) {
        super(message,e);
    }
}
