package com.progressoft.jip8.exceptions;

public class FileImportException extends RuntimeException {
    public FileImportException(String message,Exception e) {
        super(message,e);
    }
}
