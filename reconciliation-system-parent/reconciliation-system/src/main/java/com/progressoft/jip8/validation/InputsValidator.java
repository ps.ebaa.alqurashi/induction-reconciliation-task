package com.progressoft.jip8.validation;

import com.progressoft.jip8.FileValidator;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class InputsValidator implements FileValidator<SourceFile , TargetFile> {

    @Override
    // TODO rename to validate ..done
    public void validate(SourceFile sourceFile,TargetFile targetFile) throws FileNotFoundException {
        isNullPathOrFormat(sourceFile, targetFile);
        isFileValid(sourceFile, targetFile);
    }

    private void isFileValid(SourceFile sourceFile, TargetFile targetFile) throws FileNotFoundException {
        if (Files.notExists(sourceFile.getSourcePath()))
            throw new FileNotFoundException("source path doesn't a file");
        if (Files.isDirectory(sourceFile.getSourcePath()))
            throw new IllegalArgumentException("source path is directory");
        if (Files.isDirectory((targetFile.getTargetPath())))
            throw new IllegalArgumentException("target path is directory");
        if (Files.notExists(targetFile.getTargetPath()))
            throw new FileNotFoundException("target path doesn't a file");
        if (!getFileExtension(sourceFile.getSourcePath()).equals(sourceFile.getSourceFormat()))
            throw new IllegalArgumentException("source format isn't compatible with source file");
        if (!getFileExtension(targetFile.getTargetPath()).equals(targetFile.getTargetFormat()))
            throw new IllegalArgumentException("target format isn't compatible with target file");
    }

    private void isNullPathOrFormat(SourceFile sourceFile, TargetFile targetFile) {
        if (Objects.isNull(sourceFile) && Objects.isNull(targetFile))
            throw new NullPointerException("source and target are null");
        if (Objects.isNull(sourceFile))
            throw new NullPointerException("Null source path and format");
        if (Objects.isNull(targetFile))
            throw new NullPointerException("Null target path and format");

        if (Objects.isNull(sourceFile.getSourcePath())
                && Objects.isNull(sourceFile.getSourceFormat())
                && Objects.isNull(targetFile.getTargetPath())
                && Objects.isNull(targetFile.getTargetFormat()))
            throw new NullPointerException("Null inputs");
        // TODO too much checks, check sequentially .. i don't understand this
        if (Objects.isNull(sourceFile.getSourcePath()) && Objects.isNull(sourceFile.getSourceFormat()))
            throw new NullPointerException("Null source path and format");
    }

    private String getFileExtension(Path path) {
        Path fileName = path.getFileName();
        String fileFormat = fileName.toString();
        String extension = fileFormat.substring(fileFormat.indexOf(".")+1);
        return extension;
    }
}