package com.progressoft.jip8.validation;

import com.progressoft.jip8.exceptions.FileException;

import java.io.FileNotFoundException;
import java.nio.file.Path;

public class Inputs {
    private Path sourcePath;
    private String sourceFormat;
    private Path targetPath;
    private String targetFormat;

    private Inputs(Path sourcePath, String sourceFormat, Path targetPath, String targetFormat) {
        this.sourceFormat = sourceFormat;
        this.sourcePath = sourcePath;
        this.targetFormat = targetFormat;
        this.targetPath = targetPath;
    }

    public Path getSourcePath() {
        return sourcePath;
    }

    public String getSourceFormat() {
        return sourceFormat;
    }

    public Path getTargetPath() {
        return targetPath;
    }

    public String getTargetFormat() {
        return targetFormat;
    }

    // TODO use it correctly ... Done ask about it
    public static class FileBuilder {
        private Path sourcePath;
        private String sourceFormat;
        private Path targetPath;
        private String targetFormat;

        public FileBuilder(Path sourcePath, String sourceFormat, Path targetPath, String targetFormat) {
            this.sourcePath = sourcePath;
            this.sourceFormat = sourceFormat;
            this.targetPath = targetPath;
            this.targetFormat = targetFormat;
        }

        public Inputs build() {
            Inputs file = new Inputs(sourcePath, sourceFormat, targetPath, targetFormat);
            validateFileObject();
            return file;
        }

        private void validateFileObject() {
            // TODO this validation is not doing validation...done
            SourceFile sourceFile = new SourceFile(sourcePath, sourceFormat);
            TargetFile targetFile = new TargetFile(targetPath, targetFormat);
            try {
                // TODO to pass source and target files to isValidFile..done
                InputsValidator inputsValidator = new InputsValidator();
                inputsValidator.validate(sourceFile, targetFile);
            } catch (FileNotFoundException e) {
                // TODO swallowing exception...Done
                throw new FileException("file not found", e);
            }
        }
    }
}