package com.progressoft.jip8.reader;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.progressoft.jip8.exceptions.JSONFileException;
import com.progressoft.jip8.exceptions.FileNotCorrectException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class JSONReader extends FileConverterAdapter {
    protected Path path;
    private List<String> idList = new ArrayList<>();
    private List<BigDecimal> amountList = new ArrayList<>();

    public JSONReader(Path path) {
        this.path = path;
    }

    private void skipHeader(BufferedReader bf) {
        try {
            bf.readLine();
        } catch (IOException e) {
            throw new RuntimeException("no line to read", e);
        }
    }

    public String[] readConvertedJsonAsCSV(Path path) {
        Path file = path;
        try (BufferedReader br = Files.newBufferedReader(file)) {
            String line;
            String[] record = null;
            skipHeader(br);
            while ((line = br.readLine()) != null) {
                record = line.split(",");
                addToList(idList, record[0]);
                addToList(amountList, String.valueOf(new BigDecimal(record[2])));
                DateParser.isValidFormat("dd/MM/yyyy", record[5]);
            }
            return record;
        } catch (DateTimeParseException e) {
            throw new DateTimeParseException("Incorrect date format", " ", 0);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Amount is not a number");
        } catch (IOException e) {
            throw new IllegalStateException("can't read from a file", e);
        }
    }

    private void addToList(List list, String record) {
        list.add(record);
    }

    public void validateId() {
        int listSize = idList.size();
        for (int i = 0; i < listSize; i++) {
            for (int j = i + 1; j < listSize; j++) {
                if (idList.get(i).equals(""))
                    throw new NullPointerException("primary key can't be null");
                if (idList.get(i).equals(idList.get(j)))
                    throw new FileNotCorrectException("file contains duplicate Id");
            }
        }
    }

    @Override
    public Path convertToCsv(String pathName, Path convertedFilePath) {
        try {
            JsonNode jsonTree = new ObjectMapper()
                    .readTree(new File(pathName));
            JsonNode firstObject = jsonTree.elements().next();

            CsvSchema.Builder csvSchemaBuilder = CsvSchema.builder();

            // TODO in this line you should be able to get the fields in the required ordering...done
            firstObject.fieldNames().forEachRemaining(fieldName -> {
                csvSchemaBuilder.addColumn(fieldName);
            });
            CsvMapper csvMapper = new CsvMapper();
            csvSchemaBuilder.addColumn("found").build().withHeader();
            CsvSchema csvSchema;
            csvSchema = csvMapper
                    .schemaFor(JsonReorder.class)
                    .withHeader();

            csvMapper.writerFor(JsonNode.class)
                    .with(csvSchema)
                    .writeValue(new File(String.valueOf(convertedFilePath)), jsonTree);
            return convertedFilePath;
        } catch (JsonProcessingException e) {
            // TODO swallowing exception ..Done
            throw new JSONFileException("can't convert json to csv", e);
        } catch (IOException e) {
            throw new IllegalStateException("can't read json file", e);
        }
    }
}