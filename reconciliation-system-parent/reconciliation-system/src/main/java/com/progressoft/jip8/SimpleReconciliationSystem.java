package com.progressoft.jip8;

import com.progressoft.jip8.ResultWriter.FileWriter;
import com.progressoft.jip8.comparator.Comparator;
import com.progressoft.jip8.comparator.ComparatorResult;
import com.progressoft.jip8.exceptions.FileImportException;
import com.progressoft.jip8.exceptions.TableCreationException;
import com.progressoft.jip8.reader.CSVFileImporter;
import com.progressoft.jip8.reader.JSONReader;
import com.progressoft.jip8.validation.Inputs;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SimpleReconciliationSystem implements ReconciliationSystem<Inputs, String> {
    private static final String JSON = "json";
    private static final String SOURCE = "SOURCE";
    private static final String TARGET = "TARGET";

    private DataSource dataSource;
    private FileWriter writer;
    private Comparator comparator;

    public SimpleReconciliationSystem(DataSource dataSource, FileWriter writer, Comparator comparator) {
        this.dataSource = dataSource;
        this.writer = writer;
        this.comparator = comparator;
    }

    @Override
    public String reconcile(Inputs inputs) {
        try {
            importFile(SOURCE, inputs.getSourceFormat(), inputs.getSourcePath());
            importFile(TARGET, inputs.getTargetFormat(), inputs.getTargetPath());
            ComparatorResult comparatorResult = comparator.compareTransRecords();
            return writer.generateFiles(comparatorResult);
        } catch (TableCreationException e) {
            throw new TableCreationException("can't create tables", e);
        }
    }

    public ComparatorResult reconcileFiles(Inputs inputs) {
        try {
            importFile(SOURCE, inputs.getSourceFormat(), inputs.getSourcePath());
            importFile(TARGET, inputs.getTargetFormat(), inputs.getTargetPath());
            return comparator.compareTransRecords();
        } catch (TableCreationException e) {
            throw new TableCreationException("can't create tables", e);
        }
    }

    private void importFile(String fileType, String format, Path path) {
        if (format.equals(JSON)) {
            JSONReader reader = new JSONReader(path);
            // TODO result path should be according to the user
            Path convertedFilePath = Paths.get(".", "jsontoCsv.csv");
            path = reader.convertToCsv(path.toString(), convertedFilePath);
        }
        try {
            CSVFileImporter importer = new CSVFileImporter(path, dataSource);
            importer.importFile(fileType, format);
        } catch (IOException e) {
            throw new FileImportException("can't import file", e);
        }
    }
}