package com.progressoft.jip8.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class LoginServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        // TODO any validation against the credentials should be done here not in the filter ...Done
        request.getSession().setAttribute("username", username);
        request.getSession().setAttribute("password", password);

        if (!validateInfo(username, password, request)) {
            response.sendRedirect(request.getContextPath() + "/index");
            return;
        }
        response.sendRedirect(request.getContextPath() + "/upload");
    }
    private boolean validateInfo(String username, String password, HttpServletRequest req) {
        if (username == null || password == null) {
            req.getSession().setAttribute("loginFail", "User name and password are required");
            return false;
        }
        if (username.isEmpty() || password.isEmpty()) {
            req.getSession().setAttribute("loginFail", "User name and password are required");
            return false;
        }
        if (username.equalsIgnoreCase("user") && password.equalsIgnoreCase("user"))
            return true;
        req.getSession().setAttribute("loginFail", "User name or password is wrong");
        return false;
    }
}


