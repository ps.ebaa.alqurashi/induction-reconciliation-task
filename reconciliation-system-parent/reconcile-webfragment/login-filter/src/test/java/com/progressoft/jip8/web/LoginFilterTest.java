package com.progressoft.jip8.web;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class LoginFilterTest {
    @Test
    public void givenEmptyUsernameAndPassword_whenLogin_thenShouldRedirect() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getContextPath()).thenReturn("/app");
        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("username")).thenReturn("");
        when(session.getAttribute("password")).thenReturn("");
        when(request.getSession()).thenReturn(session);
        FilterChain filterChain = mock(FilterChain.class);
        LoginFilter loginFilter = new LoginFilter();
        loginFilter.doFilter(request, response,
                filterChain);
        verify(response).sendRedirect("/app/index");
    }

    @Test
    public void givenInvalidUsernameAndPassword_whenLogin_thenRedirect() throws IOException, ServletException, IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        HttpSession session = mock(HttpSession.class);

        when(request.getContextPath()).thenReturn("/app");
        when(session.getAttribute("username")).thenReturn("u");
        when(session.getAttribute("password")).thenReturn("123");
        when(request.getSession()).thenReturn(session);

        FilterChain filterChain = mock(FilterChain.class);
        LoginFilter loginFilter = new LoginFilter();
        loginFilter.doFilter(request, response, filterChain);
        verify(response).sendRedirect("/app/index");
    }

    @Test
    public void givenInvalidUsername_whenLogin_thenRedirect() throws IOException, ServletException, IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        HttpSession session = mock(HttpSession.class);

        when(request.getContextPath()).thenReturn("/app");
        when(session.getAttribute("username")).thenReturn("u");
        when(session.getAttribute("password")).thenReturn("user");
        when(request.getSession()).thenReturn(session);

        FilterChain filterChain = mock(FilterChain.class);
        LoginFilter loginFilter = new LoginFilter();
        loginFilter.doFilter(request, response, filterChain);
        verify(response).sendRedirect("/app/index");
    }

    @Test
    public void givenInvalidPassword_whenLogin_thenRedirect() throws IOException, ServletException, IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        HttpSession session = mock(HttpSession.class);

        when(request.getContextPath()).thenReturn("/app");
        when(session.getAttribute("username")).thenReturn("user");
        when(session.getAttribute("password")).thenReturn("123");
        when(request.getSession()).thenReturn(session);

        FilterChain filterChain = mock(FilterChain.class);
        LoginFilter loginFilter = new LoginFilter();
        loginFilter.doFilter(request, response, filterChain);
        verify(response).sendRedirect("/app/index");
    }
    @Test
    public void givenValidUserNameAndPassword_whenLogin_thenShouldForward() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("username")).thenReturn("user");
        when(session.getAttribute("password")).thenReturn("user");
        when(request.getSession()).thenReturn(session);

        FilterChain filterChain = mock(FilterChain.class);
        LoginFilter loginFilter = new LoginFilter();
        loginFilter.doFilter(request, response, filterChain);
        Mockito.verify(filterChain, Mockito.times(1))
                .doFilter(request, response);
    }
}
