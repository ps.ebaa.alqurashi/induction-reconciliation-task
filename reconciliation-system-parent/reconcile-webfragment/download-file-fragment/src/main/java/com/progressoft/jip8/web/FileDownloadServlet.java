package com.progressoft.jip8.web;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

public class FileDownloadServlet extends HttpServlet {

    @Override
    public void init() {
        System.out.println("init was called");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fileType = request.getParameter("fileType");
        String fileName = request.getParameter("fileName");

        throwIfNullFileData(fileName);
        throwIfNullFileData(fileType);

        Path dir = Paths.get(request.getSession().getAttribute("dir").toString());
        Path filePath = Paths.get(dir.toString(), fileName + "-transactions.csv");

        try (InputStream inputStream = Files.newInputStream(filePath);
            OutputStream out = response.getOutputStream()) {
            // TODO content type text/csv or application/json
            response.setContentType("text/plain");
            response.setHeader("Content-disposition", "attachment; filename="
                    + fileName + "." + fileType);
            if (fileType.equalsIgnoreCase("csv")) {
                byte[] buffer = new byte[1048];
                int numBytesRead;
                while ((numBytesRead = inputStream.read(buffer)) > 0) {
                    out.write(buffer, 0, numBytesRead);
                }
            } else {
                String jsonString = convertCsvToJson(inputStream);
                out.write(jsonString.getBytes());
            }
        }
    }

    private void throwIfNullFileData(String data) {
        if (Objects.isNull(data)) {
            throw new NullPointerException("empty file");
        }
    }

    private String convertCsvToJson(InputStream inputStream) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String[] headers = bufferedReader.readLine().split(",");
        String line;
        ArrayList<String> jsonObjects = new ArrayList<>();
        while ((line = bufferedReader.readLine()) != null) {
            String[] lineObject = line.split(",");
            jsonObjects.add(convertCsvLineToJsonObject(headers, lineObject));
        }
        StringBuilder stringBuilder = new StringBuilder();
        if (jsonObjects.size() > 0) {
            stringBuilder.append("[");
            for (int i = 0; i < jsonObjects.size(); i++) {
                stringBuilder.append(jsonObjects.get(i));
                stringBuilder.append(",");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append("]");
        }
        return stringBuilder.toString();
    }

    private String convertCsvLineToJsonObject(String[] headers, String[] lineObject) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        for (int i = 0; i < headers.length; i++) {
            stringBuilder.append("\"" + headers[i] + "\"" + ":" + "\"" + lineObject[i] + "\"");
            stringBuilder.append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    @Override
    public void destroy() {
        System.out.println(" Servlet out of service.");
    }
}