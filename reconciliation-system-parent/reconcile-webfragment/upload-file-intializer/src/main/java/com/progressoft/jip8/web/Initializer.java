package com.progressoft.jip8.web;

import com.progressoft.jip8.ResultWriter.CSVWriter;
import com.progressoft.jip8.ResultWriter.FileWriter;
import com.progressoft.jip8.SimpleReconciliationSystem;
import com.progressoft.jip8.comparator.Comparator;
import com.progressoft.jip8.reader.DatabaseInitializer;
import com.zaxxer.hikari.HikariDataSource;

import javax.servlet.*;
import javax.sql.DataSource;
import javax.swing.text.View;
import java.util.Set;

public class Initializer implements ServletContainerInitializer {

    public static final int MAX_FILE_SIZE = 1024 * 1024 * 10;
    public static final int FILE_SIZE_THRESHOLD = 1024 * 1024 * 2;

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        registerLoginServlet(ctx);
        registerUploadServlet(ctx);
        registerFilesDetails(ctx);
        registerCompareServlet(ctx);

        registerMatchServlet(ctx);
        registerMismatchServlet(ctx);
        registerMissingServlet(ctx);

        registerResultServlet(ctx);
        registerDownloadServlet(ctx);
    }

    private void registerMatchServlet(ServletContext ctx) {
        MatchingServlet matchingServlet = new MatchingServlet();
        ServletRegistration.Dynamic dynamic = ctx.addServlet("matchServlet", matchingServlet);
        dynamic.addMapping("/matched");
    }

    private void registerMismatchServlet(ServletContext ctx) {
        String mismatchPath = "/misMatched-transactions.csv";
        String missFilePath = "/missing-transactions.csv";
        MismatchServlet mismatchServlet = new MismatchServlet(mismatchPath, missFilePath);
        ServletRegistration.Dynamic dynamic = ctx.addServlet("mismatchServlet", mismatchServlet);
        dynamic.addMapping("/mismatch");
    }

    private void registerMissingServlet(ServletContext ctx) {
        MissingServlet missingServlet = new MissingServlet();
        ServletRegistration.Dynamic dynamic = ctx.addServlet("missingServlet", missingServlet);
        dynamic.addMapping("/missing");
    }

    private void registerResultServlet(ServletContext ctx) {
        ResultServlet resultServlet = new ResultServlet();
        ServletRegistration.Dynamic dynamic = ctx.addServlet("resultServlet", resultServlet);
        dynamic.addMapping("/result");
    }

    private void registerDownloadServlet(ServletContext ctx) {
        FileDownloadServlet servlet = new FileDownloadServlet();
        ServletRegistration.Dynamic dynamic = ctx.addServlet("downloadServlet", servlet);
        dynamic.addMapping("/download");
    }

    private void registerLoginServlet(ServletContext ctx) {
        LoginServlet servlet = new LoginServlet();
        ServletRegistration.Dynamic dynamic = ctx.addServlet("login", servlet);
        dynamic.addMapping("/index");
    }

    private void registerCompareServlet(ServletContext ctx) {
        DataSource dataSource = prepareDataSource(ctx);
        SimpleReconciliationSystem reconciliationSystem = prepareReconciliationSystem(dataSource);
        CompareServlet compareServlet = new CompareServlet(reconciliationSystem);
        ServletRegistration.Dynamic registration = ctx.addServlet("compareServlet", compareServlet);
        registration.addMapping("/compare");
    }

    private SimpleReconciliationSystem prepareReconciliationSystem(DataSource dataSource) {
        initializeDatabase(dataSource);
        Comparator comparator = new Comparator(dataSource);
        FileWriter writer = new CSVWriter();
        return new SimpleReconciliationSystem(dataSource, writer, comparator);
    }

    private void initializeDatabase(DataSource dataSource) {
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(dataSource);
        initializeDatabase(databaseInitializer);
    }

    private DataSource prepareDataSource(ServletContext ctx) {
        HikariDataSource dataSource = new HikariDataSource();
        String url = ctx.getInitParameter("url");
        String username = ctx.getInitParameter("username");
        String password = ctx.getInitParameter("password");
        dataSource.setJdbcUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    private void initializeDatabase(DatabaseInitializer databaseInitializer) {
        databaseInitializer.deleteSourceTable();
        databaseInitializer.deleteTargetTable();
        databaseInitializer.createSourceTable();
        databaseInitializer.createTargetTable();
    }

    private void registerFilesDetails(ServletContext ctx) {
        ReviewServlet details = new ReviewServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("details", details);
        registration.addMapping("/details");
    }

    private void registerUploadServlet(ServletContext ctx) {
        String view = "/WEB-INF/views/sourceUploadFile.jsp";
        String sourceSuccessPath = "/WEB-INF/views/uploadTargetFile.jsp";
        String targetSuccessPath = "/details";

        FileUploadServlet.ViewConfig sourceView = new FileUploadServlet.ViewConfig();
        sourceView.setSuccessPath(sourceSuccessPath);
        sourceView.setFileNameAttr("sourceFileName");
        sourceView.setFileTypeAttr("sourceFileType");
        sourceView.setFilePathAttr("sourceFilePath");
        FileUploadServlet sourceServlet = new FileUploadServlet(view, sourceView);
        ServletRegistration.Dynamic registration = ctx.addServlet("uploadServlet", sourceServlet);

        FileUploadServlet.ViewConfig targetView = new FileUploadServlet.ViewConfig();
        targetView.setSuccessPath(targetSuccessPath);
        targetView.setFileNameAttr("targetFileName");
        targetView.setFileTypeAttr("targetFileType");
        targetView.setFilePathAttr("targetFilePath");
        FileUploadServlet targetServlet = new FileUploadServlet(view, targetView);
        ServletRegistration.Dynamic dynamic = ctx.addServlet("uploadTarget", targetServlet);

        registration.setMultipartConfig(
                new MultipartConfigElement("", MAX_FILE_SIZE, MAX_FILE_SIZE, FILE_SIZE_THRESHOLD));
        dynamic.setMultipartConfig(
                new MultipartConfigElement("", MAX_FILE_SIZE, MAX_FILE_SIZE, FILE_SIZE_THRESHOLD));

        registration.addMapping("/upload");
        dynamic.addMapping("/targetUpload");

    }
}