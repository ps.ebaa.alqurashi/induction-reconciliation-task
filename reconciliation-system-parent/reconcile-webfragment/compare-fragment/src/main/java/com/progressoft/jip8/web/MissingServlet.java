package com.progressoft.jip8.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

//@WebServlet(urlPatterns = "/missing")
public class MissingServlet extends HttpServlet {
    private Gson gson;


    @Override
    public void init() throws ServletException {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String dir = req.getSession().getAttribute("dir").toString();
        String path = dir + "/missing-transactions.csv";

        Reader reader = Files.newBufferedReader(Paths.get(path));
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
        String[] record;
        List<MissingServlet.Transaction> transactionsList = new LinkedList<>();
        while ((record = csvReader.readNext()) != null) {
            transactionsList.add(new Transaction(record[0], record[1], record[2], record[3], record[4]));
        }
        gson.toJson(transactionsList, resp.getWriter());
    }

    private static class Transaction {
        private String found;
        private String id;
        private String amount;
        private String currency;
        private String valueDate;

        public Transaction(String found, String id, String amount, String currency, String valueDate) {
            this.found = found;
            this.id = id;
            this.amount = amount;
            this.currency = currency;
            this.valueDate = valueDate;
        }

        public String getFound() {
            return found;
        }

        public String getCurrency() {
            return currency;
        }

        public String getValueDate() {
            return valueDate;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAmount() {
            return amount;
        }
    }
}
