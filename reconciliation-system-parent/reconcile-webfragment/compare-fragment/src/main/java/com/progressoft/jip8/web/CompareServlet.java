package com.progressoft.jip8.web;

import com.progressoft.jip8.ResultWriter.CSVWriter;
import com.progressoft.jip8.SimpleReconciliationSystem;
import com.progressoft.jip8.comparator.ComparatorResult;
import com.progressoft.jip8.validation.Inputs;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CompareServlet extends HttpServlet {
    private SimpleReconciliationSystem reconciliationSystem;

    public CompareServlet(SimpleReconciliationSystem reconciliationSystem) {
        this.reconciliationSystem = reconciliationSystem;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/CompareResult.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String sourceFileType = session.getAttribute("sourceFileType").toString();
        Path sourceFilePath = Paths.get(session.getAttribute("sourceFilePath").toString());
        String targetFileType = session.getAttribute("targetFileType").toString();
        Path targetFilePath = Paths.get(session.getAttribute("targetFilePath").toString());

        Inputs inputs = new Inputs.FileBuilder(sourceFilePath, sourceFileType,
                targetFilePath, targetFileType).build();

        doCompare(req, inputs);
        resp.sendRedirect(req.getContextPath() + "/result");
    }

    private void doCompare(HttpServletRequest req, Inputs inputs) throws IOException {
        Path resultDirectory = Files.createTempDirectory("result");
        clearFiles(resultDirectory);

        req.getSession().setAttribute("dir", resultDirectory.normalize().toString());
        Path matchingPath = Files.createFile(Paths.get(resultDirectory.toString(), "matching-transactions.csv"));
        Path missMatchingPath = Files.createFile(Paths.get(resultDirectory.toString(), "misMatched-transactions.csv"));
        Path missingPath = Files.createFile(Paths.get(resultDirectory.toString(), "missing-transactions.csv"));

        ComparatorResult result = reconciliationSystem.reconcileFiles(inputs);
        writeToCsvFiles(matchingPath, missMatchingPath, missingPath, result);
        cleanLists(result);
    }

    private void clearFiles(Path resultDirectory) {
        String[] list = resultDirectory.toFile().list();
        for (String path : list) {
            File file = Paths.get(resultDirectory.normalize().toString(),path).toFile();
            if (file.exists())
                file.delete();
        }
    }

    private void writeToCsvFiles(Path matchingPath, Path missMatchingPath, Path missingPath, ComparatorResult result) {
        List<String> matchingList = result.getMatchingList();
        List<String> misMatchingList = result.getMisMatchingList();
        List<String> missingList = result.getMissingList();

        CSVWriter writer = new CSVWriter();
        writer.writeMatchingRecords(matchingList, matchingPath);
        writer.writeMissMatchingAndMissingRecords(misMatchingList, missMatchingPath);
        writer.writeMissMatchingAndMissingRecords(missingList, missingPath);
    }

    private void cleanLists(ComparatorResult result) {
        result.getMatchingList().clear();
        result.getMisMatchingList().clear();
        result.getMissingList().clear();
    }
}