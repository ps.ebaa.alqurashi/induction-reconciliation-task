package com.progressoft.jip8.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
//@WebServlet(urlPatterns = "/matched")
public class MatchingServlet extends HttpServlet {
    private Gson gson;

    @Override
    public void init() throws ServletException {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String dir = req.getSession().getAttribute("dir").toString();
        String path = dir + "/matching-transactions.csv";
        Reader reader = Files.newBufferedReader(Paths.get(path));
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
        String[] record;
        List<Transaction> transactionsList = new LinkedList<>();
        while ((record = csvReader.readNext()) != null) {
            transactionsList.add(new Transaction(record[0], record[1], record[2], record[3]));
        }
        gson.toJson(transactionsList, resp.getWriter());
    }

    private static class Transaction {
        String id;
        String amount;
        String currency;
        String valueDate;

        public Transaction(String id, String amount, String currency, String valueDate) {
            this.id = id;
            this.amount = amount;
            this.currency = currency;
            this.valueDate = valueDate;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAmount() {
            return amount;
        }
    }
}
