<%@attribute name="id" rtexprvalue="true" required="true"  type="java.lang.String" %>
<%@attribute name="href" rtexprvalue="true" required="true"  type="java.lang.String" %>
<%@attribute name="tabName" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="control" rtexprvalue="true" required="true" type="java.lang.String" %>

<a class="nav-item nav-link " id="${id}" data-toggle="tab" href="${href}"
   role="tab" aria-controls="${control}" aria-selected="true">${tabName}</a>