<body>
<section id="tabs" class="project-tab">
    <div class="container">
       <div class="row">
         <div class="col-md-12">
          <nav>
            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-match-tab" data-toggle="tab" href="#nav-match"
                 role="tab" aria-controls="nav-match" aria-selected="true">Match</a>
              <a class="nav-item nav-link" id="nav-mismatch-tab" data-toggle="tab" href="#nav-mismatch"
                 role="tab" aria-controls="nav-mismatch" aria-selected="false">Mismatch</a>
              <a class="nav-item nav-link" id="nav-missing-tab" data-toggle="tab" href="#nav-missing"
                 role="tab" aria-controls="nav-missing" aria-selected="false">Missing</a>
            </div>
          </nav>
       <div class="tab-content" id="nav-tabContent">
       <div class="tab-pane fade show active" id="nav-match" role="tabpanel" aria-labelledby="nav-match-tab">

      <table class="table" cellspacing="0">
        <thead> </thead> <tbody> </tbody>
      </table>

    </div>

    <div class="tab-pane fade" id="nav-mismatch" role="tabpanel" aria-labelledby="nav-mismatch-tab">
      <table class="table" cellspacing="0">
            <thead> </thead> <tbody </tbody>
      </table>
    </div>

    <div class="tab-pane fade" id="nav-missing" role="tabpanel" aria-labelledby="nav-missing-tab" >
      <table class="table" cellspacing="0">
         <thead> </thead>  <tbody> </tbody>
      </table>
    </div>

    </div>
    </div>
    </div>
 </div>

<a class="btn btn-primary dropdown-toggle mr-4 float-right" type="button" data-toggle="dropdown" aria-haspopup="true"
       aria-expanded="false">Download</a>
<form>
     <input class="btn btn-primary float-right" type="submit"  formaction="${pageContext.request.contextPath}/upload"
            formmethod="get" style="margin-right: 30px;" value="Compare new files">
</form>

<div class="button-box" style="margin-right: 30px;" id="btndownload">
     <div class="dropdown-menu float-right">
       <a class="dropdown-item downloadBtn" data-type="csv">CSV</a>
       <a class="dropdown-item downloadBtn" data-type="json">JSON</a>
     </div>
</div>
</section>

<script src="${pageContext.request.contextPath}/js/compare-tab.jsp"></script>