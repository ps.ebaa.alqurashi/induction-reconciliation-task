<%@page contentType="text/javascript"%>

var selectedTab = "";
    $("#nav-match-tab").on("click", function () {
        $(this).val("matched");
        $("#nav-mismatch-tab").val("");
        $("#nav-miss-tab").val("");
        selectedTab = "matching";
    });
    $("#nav-mismatch-tab").on("click", function () {
        $(this).val("mismatch");
        $("#nav-match-tab").val("");
        $("#nav-missing-tab").val("");
        selectedTab = "misMatched";
    });
    $("#nav-missing-tab").on("click", function () {
        $(this).val("missing");
        $("#nav-match-tab").val("");
        $("#nav-mismatch-tab").val("");
        selectedTab = "missing";
    });

    $("#nav-match-tab").on("click", function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/matched",
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                          var data = $.parseJSON(result);
                var headers = "<tr><th>#</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
                $("#nav-match thead").html(headers)
                $.each(data, function (i, transaction) {
                    var selector = $("#nav-match tbody");
                    if (i === 0) {
                        selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.id + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                    } else
                        selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.id + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                });
                selectedTab = "matching";
            }
        });
    });
    $("#nav-mismatch-tab").on("click", function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/mismatch",
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                var data = $.parseJSON(result);
                var headers = "<tr><th>#</th>" + "<th>" + "found in" + "</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
                $("#nav-mismatch thead").html(headers)
                $.each(data, function (i, transaction) {
                    var selector = $("#nav-mismatch tbody");
                    if (i === 0) {
                        selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.found + "</td>" + "<td>" + transaction.id + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                    } else
                        selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.found + "</td>" + "<td>" + transaction.id + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                })
            }
        });
    });
    $("#nav-missing-tab").on("click", function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/missing",
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                var data = $.parseJSON(result);
                var headers = "<tr><th>#</th>" + "<th>" + "found in" + "</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>"
                $("#nav-missing thead").html(headers)
                $.each(data, function (i, transaction) {
                    var selector = $("#nav-missing tbody");
                    if (i === 0) {
                        selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.found+ "</td>" + "<td>" + transaction.id + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                    } else
                        selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.found + "</td>" + "<td>" + transaction.id + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                })
            }
        });
    });
    var matchedHref;
    $(document).ready(function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/matched",
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                $("#nav-match-tab").val("matched");
                $("#nav-mismatch-tab").val("");
                $("#nav-missing-tab").val("");
                var data = $.parseJSON(result);
                var headers = "<tr><th>#</th>" + "<th>Transaction ID</th>" + "<th>Amount</th>" + "<th>Currency</th>" + "<th>Value date</th></tr>";
                $("#nav-match thead").html(headers);


                $.each(data, function (i, transaction) {
                    var selector = $("#nav-match tbody");
                    if (i === 0) {
                    selector.empty();
                        selector.html("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.id + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                    } else
                        selector.append("<tr> " + "<td>" + (i + 1) + "</td>" + "<td>" + transaction.id + "</td>"
                            + "<td>" + transaction.amount + "</td>" + "<td>" + transaction.currency + "</td>" + "<td>" + transaction.valueDate + "</td>" + "</tr>")
                })
            }
        });
    });

   $(".downloadBtn").on("click", function(){
        var fileType = $(this).data("type");
        var fileName = selectedTab;

     window.location.href =
          "${pageContext.request.contextPath}/download?fileName=" + fileName + "&fileType=" + fileType;
   });