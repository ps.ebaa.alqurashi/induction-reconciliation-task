package com.progressoft.jip8.web;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Path;

// TODO Rename to ReviewServlet ...Done
public class ReviewServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain");
        HttpSession session = req.getSession();
        String sourceFileName = (String) session.getAttribute("sourceFileName");
        String sourceFileType = (String) session.getAttribute("sourceFileType");
        Path sourceFilePath = (Path) session.getAttribute("sourceFilePath");

        String targetFileName = (String) session.getAttribute("targetFileName");
        String targetFileType = (String) session.getAttribute("targetFileType");
        Path targetFilePath = (Path) session.getAttribute("targetFilePath");


        req.setAttribute("sourceFileName", sourceFileName);
        req.setAttribute("sourceFileType", sourceFileType);
        req.setAttribute("sourceFilePath", sourceFilePath);

        req.setAttribute("targetFileName", targetFileName);
        req.setAttribute("targetFileType", targetFileType);
        req.setAttribute("targetFilePath", targetFilePath);


        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/fileDetails.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(req.getContextPath() + "/compare");
    }
}
