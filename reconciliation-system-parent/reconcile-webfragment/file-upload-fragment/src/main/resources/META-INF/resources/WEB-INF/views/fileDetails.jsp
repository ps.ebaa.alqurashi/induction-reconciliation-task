<body>
<div class="container">
<div class="row">
  <div class="col-md-4">
    <table class="table"><thead><tr><th scope="col"><center>Source File</center></th></tr></thead>
      <tbody>
        <tr><th scope="row">Name</th>
         <% String sourceName = (String) request.getAttribute("sourceFileName"); %>
         <td><%=sourceName%> </td>
        </tr>
        <tr> <th scope="row">Type</th>
        <%String sourceType= (String) request.getAttribute("sourceFileType"); %>
        <td> <%=sourceType%> </td></tr>
        </tbody>
     </table>

     <table class="table">
       <thead>
         <tr>
           <th scope="col"> <center> Target File </center> </th>
         </tr>
       </thead>
       <tbody>
        <tr>
          <th scope="row">Name</th>
          <% String targetName = (String) request.getAttribute("targetFileName"); %>
          <td> <%= targetName %> </td>
        </tr>
        <tr>
          <th scope="row">Type</th>
          <% String targetType= (String) request.getAttribute("targetFileType"); %>
          <td> <%= targetType %> </td>
        </tr>
       </tbody>
     </table>
  </div>
<div>
<form>
   <p> <center>Result Files format: </center></p>
     <div class="form-group">
       <select class="form-control" name="type">
         <option value="csv">CSV</option>
         <option value="json">JSON</option>
       </select>
     </div>
<input class="btn btn-primary float-right" type="submit"  formaction="${pageContext.request.contextPath}/compare"  formmethod="post" style="margin-right: 30px;" value="Compare">
<input class="btn btn-primary float-right" type="submit"  formaction="${pageContext.request.contextPath}/upload"  formmethod="get" style="margin-right: 30px;" value="cancel">
<input class="btn btn-primary float-right" type="submit"  formaction="${pageContext.request.contextPath}/targetUpload"  formmethod="get" style="margin-right: 30px;" value="back">
</form>
</div>