
<body class="text-center">
<div class="container">
    <h1>File Upload</h1>
    <up:upload-file submitUrl="${pageContext.request.contextPath}/upload" formMethod="POST"
        label="Source Name :" fileName="name" inputId="SourceFile" inputName="file" fromView="source"/>
</div>
<script>
  $(".custom-file-input").on("change", function() {
     var fileName = $(this).val().split("//").pop();
     $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>


