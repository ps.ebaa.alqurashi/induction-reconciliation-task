<body class="text-center">
<div class="container">
<h1>Target File Upload</h1>
   <up:upload-file submitUrl="${pageContext.request.contextPath}/upload" formMethod="POST"
             label="Target Name :" fileName="name" inputId="TargetFile" inputName="file" fromView="target"/>

   <button type="button"><a href="${pageContext.request.contextPath}/upload">previous</button>
</div>
<script>
  $(".custom-file-input").on("change", function() {
     var fileName = $(this).val().split("//").pop();
     $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>