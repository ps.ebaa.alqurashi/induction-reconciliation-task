<%@attribute name="submitUrl" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="formMethod" rtexprvalue="true" required="false" type="java.lang.String" %>
<%@attribute name="label" rtexprvalue="true" required="true"  type="java.lang.String" %>
<%@attribute name="inputName" rtexprvalue="true" required="true"  type="java.lang.String" %>
<%@attribute name="inputId" rtexprvalue="true" required="true"  type="java.lang.String" %>
<%@attribute name="fileName" rtexprvalue="true" required="true"  type="java.lang.String" %>
<%@attribute name="fromView" rtexprvalue="true" required="true"  type="java.lang.String" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${formMethod eq null}">
    <c:set var="formMethod" value="POST"/>
</c:if>

<form action="${submitUrl}" method="${formMethod}" enctype="multipart/form-data">
       <div class="custom-file">
             <label>${label}</label>
             <input type="text" name="${fileName}" required />
       </div>

       <div class="custom-file">
             <input type="file" name="${inputName}"  class="custom-file-input" id="${inputId}" required/>
             <label class="custom-file-label" for="${inputId}"></label>
       </div>

       <div class="form-group">
          <select class="form-control" name="type" >
               <option value="csv">csv</option>
               <option value="json">json</option>
          </select>


       </div>

       <div>
       <input type="hidden" name="fromView"  value="${fromView}"/>
       </div>
    <button type="submit" value="Upload">next</button>
 </form>