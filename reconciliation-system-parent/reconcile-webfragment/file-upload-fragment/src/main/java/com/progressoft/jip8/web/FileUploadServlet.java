package com.progressoft.jip8.web;

import org.apache.commons.io.IOUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUploadServlet extends HttpServlet {
    // TODO implement one parameterized servlet which accept the differences between both servlets...Done
    private String view;
    private ViewConfig viewConfig;

    public FileUploadServlet(String view, ViewConfig viewConfig) {
        this.view = view;
        this.viewConfig = viewConfig;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.changeSessionId();
        resp.setContentType("text/plain");
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(view);
        requestDispatcher.forward(req, resp);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final Part filePart = request.getPart("file");
        final String fileName = request.getParameter("name");
        final String fileType = request.getParameter("type");

        Path tempFile = Files.createTempFile(fileName, "." + fileType);
        IOUtils.copy(filePart.getInputStream(), Files.newOutputStream(tempFile));

        HttpSession session = request.getSession();
        session.setAttribute(viewConfig.getFileNameAttr(), fileName);
        session.setAttribute(viewConfig.getFileTypeAttr(), fileType);
        session.setAttribute(viewConfig.getFilePathAttr(), tempFile);

        response.sendRedirect(request.getContextPath() + viewConfig.getSuccessPath());

//        RequestDispatcher requestDispatcher = request.getRequestDispatcher(viewConfig.getSuccessPath());
//        requestDispatcher.forward(request, response);
    }

    public static class ViewConfig {
        private String fileNameAttr;
        private String fileTypeAttr;
        private String filePathAttr;
        private String successPath;

        public String getFileNameAttr() {
            return fileNameAttr;
        }

        public void setFileNameAttr(String fileNameAttr) {
            this.fileNameAttr = fileNameAttr;
        }

        public String getFileTypeAttr() {
            return fileTypeAttr;
        }

        public void setFileTypeAttr(String fileTypeAttr) {
            this.fileTypeAttr = fileTypeAttr;
        }

        public String getFilePathAttr() {
            return filePathAttr;
        }

        public void setFilePathAttr(String filePathAttr) {
            this.filePathAttr = filePathAttr;
        }

        public String getSuccessPath() {
            return successPath;
        }

        public void setSuccessPath(String successPath) {
            this.successPath = successPath;
        }
    }
}