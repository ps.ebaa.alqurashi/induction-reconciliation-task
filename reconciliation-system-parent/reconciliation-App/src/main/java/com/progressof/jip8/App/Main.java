package com.progressof.jip8.App;

import com.progressoft.jip8.*;
import com.progressoft.jip8.ResultWriter.CSVWriter;
import com.progressoft.jip8.ResultWriter.FileWriter;
import com.progressoft.jip8.comparator.Comparator;
import com.progressoft.jip8.reader.DatabaseInitializer;
import com.progressoft.jip8.validation.Inputs;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;


//TODO you should have more sub-packages ... (put main class in package Done)
// TODO introduce more sub-modules

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter source file location");
        Path sourcePath = Paths.get(scanner.next().trim());
        System.out.println("Enter source file format");
        String sourceFormat = scanner.next().trim().toLowerCase();
        System.out.println("Enter target file location");
        Path targetPath = Paths.get(scanner.next().trim());
        System.out.println("Enter target file format");
        String targetFormat = scanner.next().trim().toLowerCase();

        Inputs inputs = new
                Inputs.FileBuilder(sourcePath, sourceFormat, targetPath, targetFormat)
                .build();

        DataSource dataSource = getDataSource();
        Comparator comparator = new Comparator(dataSource);
        FileWriter writer = new CSVWriter();

        DatabaseInitializer databaseInitializer = new DatabaseInitializer(dataSource);
        initializeDatabase(databaseInitializer);

        SimpleReconciliationSystem reconciliationSystem =
                new SimpleReconciliationSystem(dataSource, writer, comparator);

        String resultLocation = reconciliationSystem.reconcile(inputs);

        System.out.println("Reconciliation finished.");
        System.out.println("Result files are available in directory " + resultLocation);
    }

    private static void initializeDatabase(DatabaseInitializer databaseInitializer) {
        databaseInitializer.deleteSourceTable();
        databaseInitializer.deleteTargetTable();
        databaseInitializer.createSourceTable();
        databaseInitializer.createTargetTable();
    }

    private static DataSource getDataSource() {
        String url = System.getProperty("url");
        String username = System.getProperty("username");
        String password = System.getProperty("password");
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }
}