package com.progressoft.jip8;
import java.io.IOException;
import java.sql.SQLSyntaxErrorException;

//use case
public interface ReconciliationSystem <F,P> {
    P reconcile(F inputs) throws IOException, SQLSyntaxErrorException;
}
