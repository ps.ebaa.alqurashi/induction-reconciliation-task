
package com.progressoft.jip8;


import java.io.FileNotFoundException;

public interface FileValidator<S, T> {

    void validate(S sourceFile, T targetFile) throws FileNotFoundException;
}