<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <link href="style.css" rel="stylesheet">
    <title>Login Page </title>
    <script src="${pageContext.request.contextPath}/jquery/jquery-3.4.1.js"></script>
</head>
<body class="text-center">
<h4> Login </h4>
<div class="container">
<div class="wrapper fadeInDown">
  <div id="formContent">
    <form method="POST" action="${pageContext.request.contextPath}/index">
     <div class="form-group">

     <label for ="username"> Username :</label>
     <input type="text" name="username" id="username" placeholder="Enter user name" class="fadeIn" required>

     <label for = "password"> Password :</label>
     <input type="password" name="password" id="password" placeholder="Enter password" class="fadeIn" required>

     <input type="submit" class="fadeIn fourth" value="Login">

     </div>
    </form>
   </div>
</div>
</div>
<script src="${pageContext.request.contextPath}/popper/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
